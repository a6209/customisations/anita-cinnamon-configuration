if status is-interactive
pfetch
motivate
set -g -x fish_greeting ''
starship init fish | source
alias install="sudo pacman -S"
alias search="sudo pacman -Ss"
alias rmcache="sudo pacman -Scc"
alias remove="sudo pacman -Rns"
alias uninstall="sudo pacman -Rns"
alias update="sudo pacman -Syu"
alias clean="sudo pacman -Rns (pacman -Qtdq)"
end
